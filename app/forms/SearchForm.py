from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField, IntegerField


class SearchForm(FlaskForm):
    location = SelectField(u'Choose city ', choices=[('Amsterdam', 'Amsterdam'), ('Barcelona', 'Barcelona'),
                                                             ('Berlin', 'Berlin'),('Dubai','Dubai'),("London","London"),
                                                             ('Rome', 'Rome'), ('Tuscany', 'Tuscany')],
                           render_kw={'class':'form-control', "placeholder": "Location"})
    category = SelectField(u'Choose type of entertainment', choices=[('accommodation','Accommodation'),
                                                                     ('restaurant','Restaurant'),
                                                                     ('poi','POI'),
                                                                     ('attraction','Attraction')],
                           render_kw={'class':'form-control', "placeholder": "Approach"})
    price = IntegerField('Price', render_kw={'class': 'form-control', "placeholder": "Price"})

    season = SelectField(u'Choose type of entertainment',default=0, choices=[(' ','Season'),('winter', 'winter'),
                                                                     ('summer', 'summer'),
                                                                     ('autumn', 'autumn'),
                                                                                ('spring','spring')],
                           render_kw={'class': 'form-control', "placeholder": "Approach"})
    submit = SubmitField(u'Search', render_kw={'class':'search-btn'})