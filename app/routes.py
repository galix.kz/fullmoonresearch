import os


from flask import render_template, request
import sqlite3 as sql
from app import app
from app.forms.SearchForm import SearchForm

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "../cities.db")
@app.route('/', methods=['GET','POST'])
@app.route('/index', methods=['GET','POST'])
def index():
    form = SearchForm()
    with sql.connect(db_path) as con:
        cursor = con.cursor()
        cursor.execute(
            "select distinct * from test")
        cities = cursor.fetchall()
    location_choices = []
    category_choices = []
    location_choices.append((' ','Сhoose City'))
    category_choices.append((' ', 'Choose Category'))
    for city in cities:
        if (city[0],city[0]) not in location_choices:
            location_choices.append((city[0],city[0]))
        if (city[1], city[1]) not in category_choices:
            category_choices.append((city[1], city[1]))
    form.location.choices = location_choices
    form.category.choices = category_choices
    form.location.default = location_choices[0][0];
    form.category.default = location_choices[0][0];
    form.process()
    if request.method == 'POST':
        location = request.form['location']
        category = request.form['category']
        price = request.form['price']
        print(location)
        # cursor.execute("select * from city where location='"+location+"' and category='"+category+"'")
        with sql.connect(db_path) as con:
            cursor = con.cursor()
            cursor.execute("select * from test where location='"+location+"' and category='"+category+"' and price<='"+price+"'  ORDER BY instagram_hash desc")
            # cursor.execute("select * from city")
            rows = cursor.fetchall()
            cursor.execute(
                "select * from test where location='" + location + "' ORDER BY instagram_hash desc LIMIT 10")
            # cursor.execute("select * from city")
            all = cursor.fetchall()

            print(rows)
        return render_template('page.html', form=form, rows=rows, all = all, location = location)
    return render_template('page.html', form=form)

@app.route('/form')
def login():
    form = SearchForm()
    return render_template('form.html', title='Sign In', form=form)