import os
import sqlite3

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import bs4 as bs
import time
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "./cities.db")
conn = sqlite3.connect(db_path)  # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()
cursor.execute('SELECT name, location FROM test WHERE photo is NULL ')
driver = webdriver.Chrome('./inscrawler/bin/chromedriver')
names = cursor.fetchall()


for name in names:
    driver.get("https://images.google.ru/")
    html = driver.page_source
    soup = bs.BeautifulSoup(html,'lxml')
    search = driver.find_element_by_name("q")
    search.send_keys(name[0])
    driver.find_element_by_class_name("Tg7LZd").click()
    time.sleep(1)
    html = driver.page_source
    soup = bs.BeautifulSoup(html,'lxml')
    first_image = soup.findAll("div", {"data-ri": "0"})


    if not first_image:  # if image do not exist
        print(name[0] + " could not locate first image")
        continue
    links = first_image[0].findAll("a")


    driver.get("https://www.google.com"+links[0]['href'])
    time.sleep(2)
    html = driver.page_source
    soup = bs.BeautifulSoup(html,'lxml')
    image_links = soup.findAll("img", {"class": ["irc_mi", "irc_mut"]})
    for link in image_links:
        if "src" in str(link):
            print(link['src'])
            cursor.execute(" UPDATE test SET photo = '%s' WHERE name = '%s' " % (link['src'], name[0].replace("'", '`')))
            conn.commit()
            break

input("the end? ")
driver.close()
