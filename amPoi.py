import sqlite3

import requests
from bs4 import BeautifulSoup
import os
import json
import statistics
from inscrawler import InsCrawler
import pandas as pd
import re



amAccommodation = pd.read_csv('./csv/dubai-attraction.csv')
# for name in df.loc[df['location'] == 'Amsterdam'].name.head(10):
#     name = name.lower().strip().split()[0]
#     if re.match(r'^\w+$', name):
#         print(name.lower().strip().split()[0])
conn = sqlite3.connect("./cities.db")  # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()
def is_correct(c):
    if re.match(r'^\w+$', c['name'].lower().strip().split()[0]):
        return 'correct'
    return 'incorrect'




amAccommodation['corr_str'] = amAccommodation.apply(is_correct, axis=1)
amAccommodation = amAccommodation.loc[amAccommodation['corr_str'] == 'correct']




names = amAccommodation.name.head(5)

# print(names)
ins_crawler = InsCrawler(has_screen=0)
res = ins_crawler.get_latest_posts_by_tag_2(names, 10)
rows = []
for i in range(0,10):
    row = amAccommodation.loc[amAccommodation['name'] == names.iloc[i]]
    print(row['subCategory'].values)

    subCategory = row['subCategory'].values[0]
    address = row['address'].values[0]
    name =  names.iloc[i]
    rating = res[i]
    rows.append(('Amsterdam', 'poi', subCategory, address, name, rating))
# Сохраняем изменения
cursor.executemany('insert into city values (?,?,?,?,?,?)', rows)
conn.commit()