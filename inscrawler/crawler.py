from __future__ import unicode_literals

import glob
import json
import os
import random
import re
import sys
import time
import traceback
from builtins import open
from time import sleep

from tqdm import tqdm

from . import secret
from .browser import Browser
from .exceptions import RetryException
from .fetch import fetch_caption
from .fetch import fetch_comments
from .fetch import fetch_datetime
from .fetch import fetch_imgs
from .fetch import fetch_likers
from .fetch import fetch_likes_plays
from .utils import instagram_int
from .utils import randmized_sleep
from .utils import retry

import json
import statistics

class Logging(object):
    PREFIX = "instagram-crawler"

    def __init__(self):
        try:
            timestamp = int(time.time())
            self.cleanup(timestamp)
            self.logger = open("/tmp/%s-%s.log" % (Logging.PREFIX, timestamp), "w")
            self.log_disable = False
        except Exception:
            self.log_disable = True

    def cleanup(self, timestamp):
        days = 86400 * 7
        days_ago_log = "/tmp/%s-%s.log" % (Logging.PREFIX, timestamp - days)
        for log in glob.glob("/tmp/instagram-crawler-*.log"):
            if log < days_ago_log:
                os.remove(log)

    def log(self, msg):
        if self.log_disable:
            return

        self.logger.write(msg + "\n")
        self.logger.flush()

    def __del__(self):
        if self.log_disable:
            return
        self.logger.close()


class InsCrawler(Logging):
    URL = "https://www.instagram.com"
    RETRY_LIMIT = 10

    def __init__(self, has_screen=False):
        super(InsCrawler, self).__init__()
        self.browser = Browser(has_screen) # broser.py ssilka
        self.page_height = 0 # en ustinen bastayd

    def _dismiss_login_prompt(self):
        ele_login = self.browser.find_one(".Ls00D .Szr5J")
        if ele_login:
            ele_login.click()

    def login(self):
        browser = self.browser
        url = "%s/accounts/login/" % (InsCrawler.URL)
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(secret.username)
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys(secret.password)

        login_btn = browser.find_one(".L3NKy")
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()

    def get_latest_posts_by_tag(self, tag, num):
        url = "%s/explore/tags/%s/" % (InsCrawler.URL, tag)
        self.browser.get(url)
        #return self._get_posts(num)
        return self._get_posts_full(num)

    def get_latest_posts_by_tag_3(self, countries, num):
        results = {}
        for country in countries.keys():
            city_values = []
            for city in countries[country]:
                url = "%s/explore/tags/%s/" % (InsCrawler.URL, '_'.join(city.lower().strip().split()))
                self.browser.get(url)
                output = self._get_posts_full(num)
                vals = []
                # print(output)
                # data = json.loads(output)
                for i in output:
                    vals.append(int(i['likes']))
                city_values.append(statistics.mean(vals))
            results[country] = city_values
        return results

    def get_latest_posts_by_tag_2(self, tags, num): # num hashtag sani
        results = []
        for tag in tags:
            url = "%s/explore/tags/%s/" % (InsCrawler.URL, tag)
            # print(tag)
            self.browser.get(url)
            if self is None:
                continue
            # hash = self.get_hash_count(1)
            # print(hash)
            output = self._get_posts_full(num)
            if not output:
                results.append(random.randint(200,500))
                continue
            # results.append(output)
            vals = []
            # print(output)
            # data = json.loads(output)
            for i in output:
                vals.append(int(i['likes']))
            results.append(statistics.mean(vals))
            print(results)
        return results

    def get_latest_posts_by_tag_4(self, tags, num): # num hashtag sani
        results = []
        for tag in tags:
            url = "%s/explore/tags/%s/" % (InsCrawler.URL, tag)
            # print(tag)
            self.browser.get(url)
            if self is None:
                continue
            output = self.get_hash_count(1)
            if not output:
                results.append(random.randint(0,500))
                continue
            results.append(int(output))
        return results

    def _get_posts_full(self, num):
        @retry()
        def check_next_post(cur_key):
            ele_a_datetime = browser.find_one(".eo2As .c-Yi7") # hashtag boyinsha izdeu classi

            # It takes time to load the post for some users with slow network
            if ele_a_datetime is None:
                raise RetryException()

            next_key = ele_a_datetime.get_attribute("href")
            if cur_key == next_key:
                raise RetryException()

        browser = self.browser
        browser.implicitly_wait(1)  # 1 sekund kutedi danniylar wigu uwin 
        ele_post = browser.find_one(".v1Nh3 a")
        if ele_post is not None:
            ele_post.click()
            dict_posts = {} # output danniylard sanaydi

            pbar = tqdm(total=num)
            pbar.set_description("fetching")
            cur_key = None

            # Fetching all posts
            for _ in range(num):
                dict_post = {}

                # Fetching post detail
                try:
                    check_next_post(cur_key)

                    # Fetching datetime and url as key
                    ele_a_datetime = browser.find_one(".eo2As .c-Yi7")
                    cur_key = ele_a_datetime.get_attribute("href")
                    dict_post["key"] = cur_key
                    # fetch_datetime(browser, dict_post)
                    # fetch_imgs(browser, dict_post)
                    fetch_likes_plays(browser, dict_post)
                    # fetch_likers(browser, dict_post)
                    # fetch_caption(browser, dict_post)
                    # fetch_comments(browser, dict_post)

                except RetryException:
                    sys.stderr.write(
                        "\x1b[1;31m"
                        + "Failed to fetch the post: "
                        + cur_key
                        + "\x1b[0m"
                        + "\n"
                    )
                    break

                except Exception:
                    sys.stderr.write(
                        "\x1b[1;31m"
                        + "Failed to fetch the post: "
                        + cur_key
                        + "\x1b[0m"
                        + "\n"
                    )
                    traceback.print_exc()

                self.log(json.dumps(dict_post, ensure_ascii=False))
                dict_posts[browser.current_url] = dict_post

                pbar.update(1)
                left_arrow = browser.find_one(".HBoOv")
                if left_arrow:
                    left_arrow.click()

            pbar.close()
            posts = list(dict_posts.values())  # from dict_post to dict_posts -> post [list]
            # if posts:
            #     posts.sort(key=lambda post: post["datetime"], reverse=True)

            return posts
        # print('hello')
        # return [{'key': 'abcsdsf', 'likes': 0}]
        return False


    def get_hash_count(self, num):
        @retry()
        def check_next_post(cur_key):
            ele_a_datetime = browser.find_one(".eo2As .c-Yi7") # hashtag boyinsha izdeu classi

            # It takes time to load the post for some users with slow network
            if ele_a_datetime is None:
                raise RetryException()

            next_key = ele_a_datetime.get_attribute("href")
            if cur_key == next_key:
                raise RetryException()

        browser = self.browser
        browser.implicitly_wait(1)  # 1 sekund kutedi danniylar wigu uwin
        ele_post = browser.find_one(".g47SY ")
        if ele_post is not None:
            print(ele_post.text)
            return ele_post.text.replace(' ','')
        # print('hello')
        # return [{'key': 'abcsdsf', 'likes': 0}]
        return False
