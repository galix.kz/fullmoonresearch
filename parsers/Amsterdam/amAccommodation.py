import requests
from bs4 import BeautifulSoup
import os
import json
import statistics
from inscrawler import InsCrawler
import pandas as pd
import re
import sqlite3


amAccommodation = pd.read_csv('../../csv/amsterdam-accommodation.csv')



conn = sqlite3.connect("../../mydatabase.db")  # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()


# for name in df.loc[df['location'] == 'Amsterdam'].name.head(10):
#     name = name.lower().strip().split()[0]
#     if re.match(r'^\w+$', name):
#         print(name.lower().strip().split()[0])

def is_correct(c):
    if re.match(r'^\w+$', c['name'].lower().strip().split()[0]):
        return 'correct'
    return 'incorrect'

city = "Amsterdam"


amAccommodation['corr_str'] = amAccommodation.apply(is_correct, axis=1)
amAccommodation = amAccommodation.loc[amAccommodation['location'] == city]
amAccommodation = amAccommodation.loc[amAccommodation['corr_str'] == 'correct']




names = amAccommodation.name.head(10)

# print(names)
ins_crawler = InsCrawler(has_screen=0)
res = ins_crawler.get_latest_posts_by_tag_2(names, 10)

ss = {}
for i in range(0,10):
    row = amAccommodation.loc[amAccommodation['name'] == names.iloc[i]]
    print(row['subCategory'].values)
    ss[names.iloc[i]] = {"rating": res[i], "subCategory": row['subCategory'].values[0],
                         "address": row['address'].values[0]}

    cursor.execute("""INSERT INTO albums
                      VALUES ('Amsterdam', 'accommodation', row['subCategory'].values[0],
                       row['address'].values[0],names.iloc[i], res[i])"""
                   )

# Сохраняем изменения
conn.commit()