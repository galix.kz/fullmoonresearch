import re
import sqlite3
from inscrawler import InsCrawler

conn = sqlite3.connect("./cities.db")  # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()

cursor.execute('SELECT distinct location FROM test')
names = cursor.fetchall()

for name in names:
    cursor.execute("SELECT distinct name FROM test where location='%s' " % name[0])
    places = cursor.fetchall()
    q = []
    for place in places:
        hashs = ''.join(place[0].lower().split())
        hashs.replace('`', "")
        hashs.replace('"', "")
        hashs.replace('-', "")
        hashs.replace('.', "")
        hashs = re.sub('[!@#$`"-.]', '', hashs)
        q.append(hashs)
    # q = [''.join(place.lower().split()) for place in places]
    # print(place for place in places)
    # print(q)
    ins_crawler = InsCrawler(has_screen=1)
    res = ins_crawler.get_latest_posts_by_tag_4(q, 1)
    print('_________________________________________')
    for i in range(len(places) - 1):
        print(res[i])
        s = str(places[i][0]).replace("'", '`')
        cursor.execute(" UPDATE test SET instagram_hash = %s WHERE name = '%s' " % (res[i], s))
        conn.commit()

