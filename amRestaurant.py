import sqlite3

import requests
from bs4 import BeautifulSoup
import os
import json
import statistics
from inscrawler import InsCrawler
import pandas as pd
import re

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path = os.path.join(BASE_DIR, "./csv")
for filename in os.listdir(path):
    print(filename)
    amAccommodation = pd.read_csv('./csv/' + filename)
    conn = sqlite3.connect("./cities.db")  # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()


    def is_correct(c):
        if re.match(r'^\w+$', c['name'].lower().strip().split()[0]):
            return 'correct'
        return 'incorrect'



    amAccommodation['corr_str'] = amAccommodation.apply(is_correct, axis=1)
    amAccommodation = amAccommodation.loc[amAccommodation['corr_str'] == 'correct']

    names = amAccommodation.name.head(10)

    print(names)
    ins_crawler = InsCrawler(has_screen=0)
    res = ins_crawler.get_latest_posts_by_tag_2(names, 3)

    rows = []
    for i in range(0, 10):
        print(i)
        row = amAccommodation.loc[amAccommodation['name'] == names.iloc[i]]
        print(row['subCategory'].values)

        subCategory = row['subCategory'].values[0]
        address = row['address'].values[0]
        name = names.iloc[i]
        rating = res[i]
        rows.append((row['location'].values[0], row['category'].values[0], subCategory, address, name, rating))
    # Сохраняем изменения
    cursor.executemany('insert into city values (?,?,?,?,?,?)', rows)
    conn.commit()
    conn.close()
