import sqlite3

import requests
from bs4 import BeautifulSoup
import os
import json
import statistics
from inscrawler import InsCrawler
import pandas as pd
import re


list = {'Nur-Sultan': ['Akorda', 'Atameken', 'Bayterek', 'Duman', 'Hazret Sultan Mosque', 'Han Shatyr', 'Alzhir'
            , 'Korgalzhyn National Park', 'First President Museum', 'Circus' ],
        'Almaty': ['28 Panfilov Heroes Park', 'Republics Square', 'Zelony Bazaar', 'Zenkov Cathedral'
            , 'Big Almaty Lake', 'Medeo', 'Zhetysu', 'Central State Museum', 'Kok Tobe'],
        'Baikonur': ['Baikonur', 'Baikonur Cosmodrome', 'Baikonur Museum'],
        'Taraz': ['Aisha Bibi', 'Aulie Ata', 'Babaji Khatun']}
        
conn = sqlite3.connect("./cities.db")  # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()

def is_correct(c):
    if re.match(r'^\w+$', c['name'].lower().strip().split()[0]):
        return 'correct'
    return 'incorrect'

for city in list:
    print(city)

    names = [''.join(place.lower().split()) for place in list[city]]


    ins_crawler = InsCrawler(has_screen=0)
    res = ins_crawler.get_latest_posts_by_tag_2(names, 1000)


    rows = []
    for i in range(len(names)):

        subCategory = 'touristic places'
        address = 'not available'
        name = list[city][i]
        rating = res[i]
        rows.append((city, subCategory, subCategory, address, name, rating))
    # Сохраняем изменения
    cursor.executemany('insert into city values (?,?,?,?,?,?)', rows)
    conn.commit()
